require('dotenv').config();
const mongoose = require('mongoose');

const utils = require('../utils/utils');
const data = require('./data');

mongoose.connect(process.env.CONNECTION_STRING, { useNewUrlParser: true });
const db = mongoose.connection;

db.once('open', () => {
    console.log(`${utils.getDateTime()} Database connected!`);
    data.getData('retrieved');
    //Runs every 1 hour
    setInterval(() => {
        data.getData('updated');
    }, 60 * 60 * 1000);
});

db.on('error', (error) => {
    console.log(`${utils.getDateTime()} ${error}`);
});

module.exports = mongoose;