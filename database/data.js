const request = require('request');

const Article = require('../models/articles');
const utils = require('../utils/utils');

//Get data from the URL and stored in the database
function getData(message) {
    try {
        request('https://hn.algolia.com/api/v1/search_by_date?query=nodejs', (error, response, body) => {
            console.log(`${utils.getDateTime()} Data ${message} from server! Articles(${JSON.parse(response.body)['hits'].length})`);
            Article.collection.insertMany(JSON.parse(response.body)['hits'], {ordered : false }).then(() => {
                console.log(`${utils.getDateTime()} Data stored in database! Articles(${JSON.parse(response.body)['hits'].length})`);
            });
        });
    } catch (error) {
        console.log(`${utils.getDateTime()} Error:`, error.message);
    }
}

module.exports = { getData };