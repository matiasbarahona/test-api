const express = require('express');
const router = express.Router();

const Article = require('../models/articles');
const utils = require('../utils/utils');

//Get a filtered and paginated list of articles (5 max) from the database
router.get('/getArticles', async (req, res) => {
    try {
        let query = {};
        if (req.query.author) {
            query.author = req.query.author;
        }
        if (req.query.title) {
            query.title = req.query.title;
        }
        if (req.query._tags) {
            query._tags = {
                $in: req.query._tags
            };
        }
        const articles = await Article.find(query).limit(5).skip((req.query.page - 1) * 5);
        if (articles.length > 0) {
            res.status(200).send(articles);
            console.log(`${utils.getDateTime()} Data retrieved from database! Page(${req.query.page}) / Articles(${articles.length})`);
        } else {
            res.status(200).send({ status: 'No data found!' });
            console.log(`${utils.getDateTime()} No data found! Page(${req.query.page}) / Articles(${articles.length})`);
        }
    } catch (error) {
        res.status(500).send({ status: error.message });
        console.log(`${utils.getDateTime()} Error:`, error.message);
    }
});

//Remove an article from the database based on a ID, that article doesn't show again
router.delete('/removeArticle', async (req, res) => {
    try {
        const article = await Article.find({ objectID: req.query.objectID });
        if (article.length > 0) {
            await Article.deleteOne({ objectID: req.query.objectID });
            res.status(200).send({ message: `Article ${req.query.objectID} removed!` });
            console.log(`${utils.getDateTime()} Article ${req.query.objectID} removed!`);
        } else {
            res.status(200).send({ status: `No data found! Article ${req.query.objectID} not removed!` });
            console.log(`${utils.getDateTime()} No data found! Article ${req.query.objectID} not removed!`);
        }
    } catch (error) {
        res.status(500).send({ status: error.message });
        console.log(`${utils.getDateTime()} Error:`, error.message);
    }
})

module.exports = router;