const mongoose = require('mongoose');

const articlesSchema = new mongoose.Schema({
    title: String,
    author: String,
    _tags: [{
        type: String,
    }],
    objectID: String
});

module.exports = mongoose.model('Article', articlesSchema, 'Articles');
