require('dotenv').config();
const express = require('express');
const app = express();
const conn = require('./database/conn');
const utils = require('./utils/utils');

const articlesRouter = require('./routes/articles');
app.use('/articles', articlesRouter);

app.listen(process.env.PORT, process.env.ADRESS, () => {
    console.clear();
    console.log(`${utils.getDateTime()} Server started! in`, process.env.ADRESS, process.env.PORT);
});

app.get('/', (req, res) => {
    res.status(200).send({
        status: 'Ok'
    });
});

app.get('*', (req, res) => {
    res.status(404).send({
        status: 'Not found'
    });
});