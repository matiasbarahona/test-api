//Return a formatted date/time
function getDateTime() {
    let currentDate = new Date();
    let year = currentDate.getFullYear();
    let month = currentDate.getMonth() + 1;
    let day = currentDate.getDate();
    let hours = currentDate.getHours();
    let minutes = currentDate.getMinutes();
    let seconds = currentDate.getSeconds();
    let formatDate = `[${day}/${month}/${year} ${hours}:${minutes}:${seconds}]`;
    return formatDate;
}

module.exports = { getDateTime };