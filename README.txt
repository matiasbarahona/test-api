Test API

Prerequisites:
    NodeJS
    MongoDB
    Postman

How to use:

1) Start the MongoDB service.
2) Import 'REIGN.postman_collection' file into Postman.
3) Run 'npm start' from the project folder CMD.
4) Make request from Postman.
